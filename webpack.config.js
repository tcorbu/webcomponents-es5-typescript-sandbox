const fs = require('fs');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
require.extensions['.txt'] = function(module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

const banner = require('./banner.txt');
console.log(banner);

module.exports = {
  entry: {
    app: [
      './src/main.ts',
      './src/main.scss'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'tcCustomComponents'
  },

  resolve: {
    extensions: ['.ts', '.js', '.scss', '.svg']
  },
  module: {
    rules: [{
        test: /\.ts$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.scss/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            'sass-loader',
            'resolve-url-loader'
          ]
        })
      },
      {
        test: /\.md$/,
        use: [{
            loader: "html-loader"
          },
          {
            loader: "markdown-loader",
            options: {
              /* your options here */
            }
          }
        ]
      },
      {
        test: /\.pug/,
        use: {
          loader: "pug-loader",
          options: {}
        }
      }, {
        test: /\.svg$/,
        use: [{
            loader: 'svg-sprite-loader',
            /*options: { ... }*/
          },
          //'svg-fill-loader',
          //'svgo-loader'
        ]
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin(banner),
    // new UglifyJSPlugin(),
    new ExtractTextPlugin('app.css'),
    new HtmlWebpackPlugin({
      title: 'WebComponents example',
      template: './assets/main.html'
    }),
    new SpriteLoaderPlugin()
  ]
};
