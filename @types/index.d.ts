declare type ShadowModes = 'open' | 'close';

declare interface ShadowModifiers {
    mode: ShadowModes;
}

declare interface DocumentOrShadowRoot {
    readonly activeElement: Element | null;
    readonly stylesheets: StyleSheetList;

    getSelection(): Selection | null;

    elementFromPoint(x: number, y: number): Element | null;

    elementsFromPoint(x: number, y: number): Element[];
}


declare interface ShadowRoot extends DocumentOrShadowRoot, DocumentFragment {
    readonly host: Element;
    innerHTML: string;
}

declare interface HTMLElement {
    attachShadow(shadowModifiers: ShadowModifiers);
}

declare interface ElementDefinitionOptions {
    extends: string;
}


declare interface CustomElementRegistry {
    define(name: string, constructor: Function, options?: ElementDefinitionOptions): void;

    get(name: string): any;

    whenDefined(name: string): PromiseLike<void>;
}

declare interface Window {
    customElements: CustomElementRegistry;
}

declare interface Element {
    readonly shadowRoot: ShadowRoot;
}

declare function require(id: string) : any;