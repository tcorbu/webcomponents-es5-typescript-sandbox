export class HtmlToElementConverter {

    public static convert(html) {
        let template = document.createElement('template');
        template.innerHTML = html;
        const node = template.content.cloneNode(true);
        return node;
    }

}