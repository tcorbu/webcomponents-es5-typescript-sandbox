import './CustomElement';
import {EventInterceptor, EventInterceptorRegistry} from "./00-ShowcaseInterceptor/EventInterceptorRegistry";

// Create a class for the element


export class Application implements EventInterceptor {

    private interceptorRegistry: EventInterceptorRegistry;

    public constructor() {
        this.interceptorRegistry = new EventInterceptorRegistry();
        this.interceptorRegistry.addInterceptor(this);

    }

    public registerComponents() {

    }

    interceptEvent(event, metadata): boolean {
        console.log('Intercepted event', event, metadata);
        return false;
    }
}