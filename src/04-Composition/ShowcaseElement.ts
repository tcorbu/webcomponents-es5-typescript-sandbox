import {BaseElement} from "../01-Start/BaseElement";

export class ShowcaseElement extends BaseElement {

    static template: Function = require('./ShowcaseTemplate.pug');
    private elementScope: ShadowRoot;

    static get is(): string {
        return 'tc-showcase';
    }

    constructor() {
        super();
    }

    connectedCallback() {
        this.elementScope = this.attachShadow({mode: 'open'});
        this.elementScope.innerHTML = ShowcaseElement.template();
    }
}

window.customElements.define(ShowcaseElement.is, ShowcaseElement);