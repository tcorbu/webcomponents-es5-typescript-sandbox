import {BaseElement} from "../01-Start/BaseElement";

export class LayoutElement extends BaseElement {

    static template: Function = require('./LayoutTemplate.pug');
    private elementScope: ShadowRoot;

    static get is(): string {
        return 'tc-layout';
    }

    constructor() {
        super();
    }

    connectedCallback() {
        this.elementScope = this.attachShadow({mode: 'open'});
        this.elementScope.innerHTML = LayoutElement.template();
    }


}

window.customElements.define(LayoutElement.is, LayoutElement);