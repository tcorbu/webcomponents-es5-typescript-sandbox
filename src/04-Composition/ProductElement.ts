import {BaseElement} from "../01-Start/BaseElement";

export class ProductElement extends BaseElement {

    static template: Function = require('./ProductTemplate.pug');
    private elementScope: ShadowRoot;

    static get is(): string {
        return 'tc-product';
    }

    constructor() {
        super();
    }

    set product(product){
        this.setAttribute('product', JSON.stringify(product));
    }

    get product(){
        let attribute = this.getAttribute('product');
        return JSON.parse(attribute);
    }

    public static get observedAttributes() {
        // Monitor the 'product' attribute for changes.
        return ['product'];
    }

    attributeChangedCallback(){

    }

    connectedCallback() {
        this.elementScope = this.attachShadow({mode: 'open'});
        this.elementScope.innerHTML = ProductElement.template();
    }
}

window.customElements.define(ProductElement.is, ProductElement);