import {BaseElement} from "../01-Start/BaseElement";
import {ButtonElement} from "../02-Extension/ButtonElement";


export class CounterElement extends BaseElement {

    private incrementButton: ButtonElement;
    private decrementButton: ButtonElement;
    private incrementClickListener: EventListenerOrEventListenerObject;
    private decrementClickListener: EventListenerOrEventListenerObject;
    static template: Function = require('./CounterTemplate.pug');
    private elementsScope: ShadowRoot;

    private get displayLabel(): HTMLLabelElement {
        return this.elementsScope.querySelector('span') as HTMLLabelElement;
    };

    constructor() {
        super();
    }

    static get is() {
        return 'tc-counter';
    }

    static get observedAttributes() {
        return ['value'];
    }

    public connectedCallback() {
        super.connectedCallback.apply(this, arguments); // Overload
        this.createShadow();

        this.decorateElements();
        if (!this.hasAttribute('value')) {
            this.setAttribute('value', this.min);
        }
    }


    disconnectedCallback() {
        super.disconnectedCallback.apply(this, arguments); // Overload
        this.incrementButton.removeEventListener('click', this.incrementClickListener);
        this.decrementButton.removeEventListener('click', this.decrementClickListener);
    }

    private createShadow() {
        this.elementsScope = this.attachShadow({mode: 'open'});
        this.elementsScope.innerHTML = CounterElement.template();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        super.attributeChangedCallback.apply(this, arguments); // Overload
        this.displayLabel.innerText = this.value + '';
    }

    get value(): string {
        return this.getAttribute('value') || this.min;
    }

    get step() {
        return this.getAttribute('step');
    }

    get min() {
        return this.getAttribute('min') || '0';
    }

    get max() {
        return this.getAttribute('max') || '100';
    }

    set value(newValue: string) {
        this.setAttribute('value', newValue);
    }

    set step(newValue) {
        this.setAttribute('step', newValue);
    }

    set min(newValue) {
        this.setAttribute('min', newValue);
    }

    set max(newValue) {
        this.setAttribute('max', newValue);
    }

    private decorateElements() {
        this.decorateIncrementButton();
        this.decorateDecrementButton();
    }

    private decorateIncrementButton() {
        this.incrementButton = this.elementsScope.querySelector('tc-button.increment') as ButtonElement;
        console.log('stuff');
        this.incrementClickListener = this.incrementValue.bind(this);
        this.incrementButton.addEventListener('click', this.incrementClickListener);
    }


    private decorateDecrementButton() {
        this.decrementButton = this.elementsScope.querySelector('tc-button.decrement') as ButtonElement;
        this.decrementClickListener = this.decrementValue.bind(this);
        this.decrementButton.addEventListener('click', this.decrementClickListener);
    }

    private incrementValue() {
        // using +myVariable coerces myVariable into a number,
        // we do this because the attribute's value is received as a string
        const step = +this.step || 1;
        const newValue = +this.value + step;

        if (this.max) {
            const numberValue = newValue > +this.max ? +this.max : +newValue;
            this.value = numberValue.toString(10);
        } else {
            this.value = newValue.toString(10);
        }
    }

    private decrementValue() {
        const step = +this.step || 1;
        const newValue = +this.value - step;

        if (this.min) {
            const numberValue = newValue > +this.max ? +this.max : +newValue;
            this.value = numberValue.toString(10);
        } else {
            this.value = newValue.toString(10);
        }

    }

}

window.customElements.define(CounterElement.is, CounterElement);