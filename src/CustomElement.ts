// Define the class for a new element called custom-element

export class CustomElement extends HTMLElement {

    private rootElement: ShadowRoot;
    
    static get is() {
        return "custom-element";
    }

    constructor() {
        super();
    }

    public connectedCallback(){
        this.textContent = 'I am a web component';

        this.rootElement = this.attachShadow({mode: 'open'});
        const template = document.querySelector('#CustomElementTemplate') as HTMLTemplateElement;
        const clone = document.importNode(template.content, true);
        this.rootElement.appendChild(clone);
    }

    // Monitor the 'name' attribute for changes.
    static get observedAttributes() {
        return ['value'];
    }
    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr == 'value') {
             this.rootElement.textContent=newValue;
        }
    }
    public disconnectedCallback(){

    }
}

// Register the new element with the browser
window.customElements.define('custom-element', CustomElement);