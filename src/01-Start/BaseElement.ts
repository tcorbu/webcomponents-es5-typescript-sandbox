import {EventInterceptor, EventInterceptorRegistry} from "../00-ShowcaseInterceptor/EventInterceptorRegistry";

export class BaseElement extends HTMLElement {
    private _version: string;

    protected eventInterceptor: EventInterceptor;


    public static get is() {
        return 'tc-base'
    }

    public static get observedAttributes() {
        // Monitor the 'name' attribute for changes.
        return ['version'];
    }

    constructor() {
        super();
        this.eventInterceptor = new EventInterceptorRegistry();
        this._version = '0.1';
        this.eventInterceptor.interceptEvent({eventType: 'constructed', name: this.constructor.name});
    }

    public connectedCallback() {
        // Called when the element is inserted into a document, including into a shadow tree
        this.eventInterceptor.interceptEvent({eventType: 'connectedCallback', name: this.constructor.name});
    }

    public disconnectedCallback() {
        // Called when the element is removed from a document
        this.eventInterceptor.interceptEvent({eventType: 'disconnectedCallback', name: this.constructor.name});
    }

    attributeChangedCallback(attributeName: string, oldValue: string, newValue: string, namespace: string) {
        // Called when an attribute is changed, appended, removed, or replaced on the element. Only called for observed attributes.
        this.eventInterceptor.interceptEvent({
            eventType: 'attributeChangedCallback',
            name: this.constructor.name,
            attributes: Array.prototype.splice.call(this, arguments)
        })
    }

    adoptedCallback(oldDocument, newDocument) {
        // Called when the element is adopted into a new document
        this.eventInterceptor.interceptEvent({
            eventType: 'adoptedCallback',
            name: this.constructor.name,
            attributes: Array.prototype.splice.call(this, arguments)
        })
    }

    public get version() {
        return this._version;
    }
}

// Register the new element with the browser
window.customElements.define(BaseElement.is, BaseElement);