import './polyfils';

export * from './CustomElement';

export * from './00-Assets';
export * from './01-Start';
export * from './02-Extension';
export * from './03-Attributes';
export * from './04-Composition';

import {Application} from './Application';
new Application();
