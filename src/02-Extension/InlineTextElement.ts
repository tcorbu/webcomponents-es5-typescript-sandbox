import {BaseElement} from "../01-Start/BaseElement";

class InlineTextElement extends BaseElement {

    static get is(): string {
        return 'tc-inline-text';
    }

    constructor() {
        super();
    }


}

// Register the new element with the browser
window.customElements.define(InlineTextElement.is, InlineTextElement);