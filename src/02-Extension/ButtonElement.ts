import {BaseElement} from "../01-Start/BaseElement";
import {HtmlToElementConverter} from "../WebComponentsHelpers/HtmlToElementConverter";
const template = require('./ButtonTemplate.pug');


export class ButtonElement extends BaseElement { // Impossible at the moment to extend HTMLButtonElement

    private clickListener: EventListenerOrEventListenerObject;
    private initialParentPosition: string | any;
    private elementScope: ShadowRoot;


    static get is(): string {
        return 'tc-button';
    }

    constructor() {
        super();
    }

    connectedCallback() {
        this.initialParentPosition = this.parentElement.style.position;
        this.parentElement.style.position = 'relative';
        this.elementScope = this.attachShadow({mode: 'open'});
        const node = HtmlToElementConverter.convert(template());
        this.elementScope.appendChild(node);

    }

    disconnectedCallback() {
        // revert changes
        this.parentElement.style.position = this.initialParentPosition;
        this.parentElement.removeEventListener('click', this.clickListener);
    }

}

// Register the new element with the browser
window.customElements.define(ButtonElement.is, ButtonElement);