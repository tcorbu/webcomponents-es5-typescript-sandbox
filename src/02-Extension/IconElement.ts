import { BaseElement } from "../01-Start/BaseElement";
import { HtmlToElementConverter } from "../WebComponentsHelpers/HtmlToElementConverter";
const template = require('./IconTemplate.pug');


export class IconElement extends BaseElement { // Impossible at the moment to extend HTMLButtonElement

  private elementScope: ShadowRoot;


  static get is(): string {
    return 'tc-icon';
  }

  constructor() {
    super();
  }

  connectedCallback() {
    this.elementScope = this.attachShadow({ mode: 'open' });
    const node = HtmlToElementConverter.convert(template());
    this.elementScope.appendChild(node);

  }

  disconnectedCallback() {
    // revert changes
  }

  attributeChangedCallback(attributeName: string, oldValue: string, newValue: string, namespace: string) {
    // Called when an attribute is changed, appended, removed, or replaced on the element. Only called for observed attributes.

  }


  public static get observedAttributes() {
    // Monitor the 'name' attribute for changes.
    return ['icon-set', 'size', 'name'];
  }
}
console.log('define ', IconElement.is);
// Register the new element with the browser
window.customElements.define(IconElement.is, IconElement);
