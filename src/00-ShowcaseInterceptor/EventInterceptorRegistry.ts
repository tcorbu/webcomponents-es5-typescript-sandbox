export interface EventInterceptor {
    interceptEvent(event:any, payload?:any): boolean;
}

export class EventInterceptorRegistry implements EventInterceptor {


    private static interceptors: Array<EventInterceptor> = [];

    private indexOfInterceptor(interceptor: EventInterceptor): number {
        return EventInterceptorRegistry.interceptors.indexOf(interceptor);
    }

    public addInterceptor(interceptor: EventInterceptor) {
        if (!~this.indexOfInterceptor(interceptor)) {
            EventInterceptorRegistry.interceptors.push(interceptor);
        }
    }

    public removeInterceptor(interceptor: EventInterceptor) {
        const idx = this.indexOfInterceptor(interceptor);
        if (~idx) {
            EventInterceptorRegistry.interceptors.splice(idx, 1);
        }
    }

    public interceptEvent(event, metadata): boolean {
        let stopEvent = false;
        for (const interceptor of EventInterceptorRegistry.interceptors) {
            stopEvent = interceptor.interceptEvent(event, metadata);
            if (stopEvent) {
                break;
            }
        }
        return stopEvent;

    }

}