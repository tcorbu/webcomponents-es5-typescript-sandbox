var webpackConfig = require('./webpack.conf.js');

module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
      './**/*.spec.ts'

    ],
    proxies: {},
    exclude: [],
    preprocessors: {
      './*.ts': ['webpack']
    },
    webpack: webpackConfig,
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    mime: {
      'text/x-typescript': ['ts', 'tsx']
    },
    singleRun: true,
    concurrency: Infinity,
    plugins: [
      'karma-chrome-launcher',
      'karma-sourcemap-loader',
      'karma-webpack',
      'karma-jasmine'
    ]
  });
};
